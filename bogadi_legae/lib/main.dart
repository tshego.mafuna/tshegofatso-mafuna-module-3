import 'package:bogadi_legae/pages/dashboard.dart';
import 'package:bogadi_legae/pages/login.dart';
import 'package:bogadi_legae/pages/profile_edit.dart';
import 'package:bogadi_legae/pages/register.dart';
import 'package:bogadi_legae/pages/welcome_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bogadi Legae',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const WelcomeScreen(),
      routes: {
        '/': (context) => const WelcomeScreen(),
        '/dashboard': (context) => const Dashboard(),
        '/register': (context) => const Register(),
        '/profilepage': (context) => const ProfilePage(),
        '/login': (context) => const LoginScreen(),
      },
    );
  }
}
