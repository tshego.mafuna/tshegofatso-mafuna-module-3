import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bogadi Legae"),
      ),
      body: Container(
        width: 380,
        margin: const EdgeInsets.all(10.0),
        child: const Text("Dashboard"),
      ),
    );
  }
}
