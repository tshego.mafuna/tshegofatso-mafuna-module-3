import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bogadi Legae"),
      ),
      body: Container(
        width: 380,
        margin: const EdgeInsets.all(10.0),
        child: const Text("Profile"),
      ),
    );
  }
}
