import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bogadi Legae"),
      ),
      body: Container(
        width: 380,
        margin: const EdgeInsets.all(10.0),
        child: const Text("Login Screen"),
      ),
    );
  }
}
